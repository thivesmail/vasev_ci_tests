<?php

namespace App\Tests;

use App\MessageGenerator;
use \PHPUnit\Framework\TestCase;

class MessageGeneratorTest extends TestCase
{
    public function testGetMessage()
    {
        $this->assertEquals('Hello, 0!',  MessageGenerator::getMessage());
    }
}
