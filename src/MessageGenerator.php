<?php

namespace App;

class MessageGenerator
{
    /**
     * @return string
     */
    public static function getMessage()
    {
        return "Hello, " . rand(0, 1) . "!";
    }
}